package com.digital.java.code.assignment.books.rest.resource;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.digital.java.code.assignment.books.model.Book;
import com.digital.java.code.assignment.books.services.BookService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest
public class BookResourceTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private BookService bookService;
	
	private Book dummykBook;
	
	@Before
	public void setup() {
		dummykBook = new Book(); //As Jackson not like Mocks 
		dummykBook.setIsbn("6354");
	}
	
	
	@Test
    public void findBookByValidId_ThenSucessTest() throws Exception {
        when(bookService.getBook(dummykBook.getIsbn())).thenReturn(dummykBook); //Mocking the service call
        
        this.mockMvc.perform(get("/books/" + dummykBook.getIsbn()))
        			.andExpect(status().isOk())
        			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        			.andExpect(jsonPath("$.isbn").value(dummykBook.getIsbn()));
        verify(bookService).getBook(dummykBook.getIsbn());
      }
	
	@Test
    public void findBookByInValidId_ThenExceptionHandleTest() throws Exception {
        when(bookService.getBook(dummykBook.getIsbn())).thenReturn(null); //Mocking the service call. null obj found
        
        this.mockMvc.perform(get("/books/" + "invalidId"))
        			.andExpect(status().isNotFound())
        			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
      }
	
	
	@Test
    public void saveBookWithMmandatoryParamsFilled_ThenSucessTest() throws Exception {
		dummykBook.setTitle("Spring Boot");
		dummykBook.setAuthour("Tim");
		
	    when(bookService.persist(dummykBook)).thenReturn(1L); //Mocking the service call
        
        
        this.mockMvc.perform(post("/books")
        			.contentType(MediaType.APPLICATION_JSON_VALUE)
        			.content(getJasonStringFor(dummykBook)))
        			.andExpect(status().isOk())
        			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        			.andExpect(jsonPath("$.operation").value("SAVE"))
        			;
        verify(bookService).persist(dummykBook);
      }
	
	private String getJasonStringFor(final Object object) throws JsonProcessingException {
		ObjectMapper jasonMapper = new ObjectMapper();
		return jasonMapper.writeValueAsString(object);
	}
}
