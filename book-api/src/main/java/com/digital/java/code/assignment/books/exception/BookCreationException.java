package com.digital.java.code.assignment.books.exception;

public class BookCreationException extends RuntimeException {

	public BookCreationException(String message) {
		super(message);
	}
}
