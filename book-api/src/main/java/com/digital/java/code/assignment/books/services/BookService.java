package com.digital.java.code.assignment.books.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digital.java.code.assignment.books.dao.BookDao;
import com.digital.java.code.assignment.books.model.Book;

@Service
public class BookService {

	@Autowired
	private BookDao bookDao;

	public Book getBook(final String isbn) {

		return bookDao.findBookById(isbn);
	}

	/**
	 * Persist the new 'Book' entity into database.
	 * 
	 * @param newBook
	 *            Book to persist
	 * @return new Id if successes. -1 if failed.
	 */
	public long persist(Book book) {

		validateMandatory(book);
		return bookDao.persist(book);
		
	}

	private void validateMandatory(final Book book) {
		assertNotNullAndNotEmpty("ISBN", book.getIsbn());
		assertNotNullAndNotEmpty("Title", book.getTitle());
		assertNotNullAndNotEmpty("Author", book.getAuthour());

	}

	private void assertNotNullAndNotEmpty(final String propertyName, final String value) {
		if (value == null) {
			throw new IllegalArgumentException(propertyName + " must be not null");
		}
		
		if(value.isEmpty()) {
			throw new IllegalArgumentException(propertyName + " must be not empty");
		}
	}
	
	public List<Book> getAllBooks() {
		
		return bookDao.findAllBooks();
	}

	public void setBookDao(BookDao bookDao) { //To inject MockDao
		this.bookDao = bookDao;
	}
}
