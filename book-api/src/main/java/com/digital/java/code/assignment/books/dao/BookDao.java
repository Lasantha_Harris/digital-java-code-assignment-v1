package com.digital.java.code.assignment.books.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.digital.java.code.assignment.books.model.Book;
import com.digital.java.code.assignment.books.persistent.BookStore;

@Repository
public class BookDao {

	@Autowired
	private BookStore bookStore;

	public Book findBookById(String isbn) {

		return bookStore.getBooks().get(isbn);
	}

	/**
	 * Persist the new 'Book' entity into database.
	 * 
	 * @param newBook
	 * @return new Id if successes. -1 if failed.
	 */
	public long persist(final Book newBook) {
		return bookStore.persist(newBook);

	}

	public List<Book> findAllBooks() {
		
		return bookStore.getAll();
	}

}
