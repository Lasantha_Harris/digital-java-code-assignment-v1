package com.digital.java.code.assignment.books.exception;

public class BookNotFoundException extends RuntimeException {

	public BookNotFoundException(String message) {
		super(message);

	}

}
