package com.digital.java.code.assignment.books.persistent;

import java.time.LocalDate;
import java.time.Month;

import com.digital.java.code.assignment.books.model.Book;
import com.digital.java.code.assignment.books.model.BookBuilder;
import com.digital.java.code.assignment.books.model.BookDetails;
import com.digital.java.code.assignment.books.model.BookDetailsBuilder;

public class Inventory {
	
	static int ID = 1;

	static final BookDetails DETAILS = new BookDetailsBuilder().withDescription("Des").withLanuage("English")
			.withNoOfPages((short) 345).withPublisher("O'really")
			.withPublishedDate(LocalDate.of(2011, Month.FEBRUARY, 7).toString()).create();

	// Sample Book1
	static final Book BOOK1 = new BookBuilder().withBookId(ID++).withIsbn("45-124").withAuthour("Sam Newman")
			.withTitle(" Building Microservices").withPrice("$32.77").withBookDetails(DETAILS).create();

	// Sample Book2
	static final Book BOOK2 = new BookBuilder().withBookId(ID++).withIsbn("67-8656").withAuthour("Sam Newman")
			.withTitle(" Head First Design Pattern").withPrice("$32.77").create();

}
