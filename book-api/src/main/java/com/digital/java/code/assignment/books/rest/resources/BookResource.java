package com.digital.java.code.assignment.books.rest.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digital.java.code.assignment.books.exception.BookCreationException;
import com.digital.java.code.assignment.books.exception.BookNotFoundException;
import com.digital.java.code.assignment.books.model.Book;
import com.digital.java.code.assignment.books.rest.response.SuccessMessage;
import com.digital.java.code.assignment.books.services.BookService;

@RestController
public class BookResource {
	
	@Autowired
	private BookService bookService;
	
	@RequestMapping("/books")
	public List<Book> getAllBooks() {
		
		return bookService.getAllBooks();
	}
	

	@RequestMapping("/books/{isbn}")
	public Book getBook(@PathVariable final String isbn) {
		
		Book book = bookService.getBook(isbn);
		if(book == null) 
		{
			throw new BookNotFoundException("No book found for ISBN: " + isbn);
		}
		return book;
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/books")
	public SuccessMessage saveBook(@RequestBody final Book newBook) 
	{
		long newId = bookService.persist(newBook);
		
		if(newId < 0) {
			throw new BookCreationException("Error in creating book wiht ISBN: " + newBook.getIsbn());
		}
		return new SuccessMessage("SAVE","New Book is saved with ISBN: " + newBook.getIsbn());
	}

}
