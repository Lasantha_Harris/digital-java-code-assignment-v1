package com.digital.java.code.assignment.books.exception;

public class DuplicateBookException extends RuntimeException {

	public DuplicateBookException(String message) {
		super(message);
	}

}
